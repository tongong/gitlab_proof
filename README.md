# Gitlab Proof
This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:2A1E4A37EC5776CD1104AFB0BB7591245A64B2FC]
